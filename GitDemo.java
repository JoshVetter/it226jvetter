package edu.isu.it226;

/**
 * Use this class for making multiple commits. For example,
 * fill in each function and make a commit
 * @author rsaripa
 *
 */

public class GitDemo {

	public int getPermutation(int value){

		if (value == 0)
		{
			return 0;
		}
		
		
		return  value + getPermutation(value - 1); 

	}
	
	public int getFibonacci(int value){
		
		if(value == 1 || value == 2)
		{
			return 1;
		}
		
		return getFibonacci(value - 1) + getFibonacci(value - 2); 
	}
	
	public int getLeastCommonDenominator(int x, int y){

		
		if(x > y)
		{
			if(x % y == 0)
			{
				return y;
			}
			
			return x * y;
		}
		
		if(y % x == 0)
		{
			return x;
		}
		
		return x*y;
	}

	public int bowlingPinRows(int rows)
	{
			
		//Loop through n-1 until n=1
		
		if(rows == 1)
		{		
			return 1;	
		}
		
		else
		{
			return (rows + bowlingPinRows(rows - 1));
		}
	}
	
	public int addNumbers(int a, int b)
	{
		
		return a + b;
	}
	
	
}
