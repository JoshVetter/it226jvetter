package edu.isu.it226;

public class GitDemoTest {

	public static void main(String[] args) {
		
		GitDemo demo = new GitDemo();
		
	
		System.out.println(demo.getPermutation(5));
		System.out.println(demo.getFibonacci(7));
		System.out.println(demo.getLeastCommonDenominator(6, 12));
		System.out.println(demo.bowlingPinRows(10));
		System.out.println(demo.addNumbers(11, 17));
	}

}
